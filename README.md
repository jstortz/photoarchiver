# Photoarchiver

I created this gem to do some ultra simple photo organization.  This will read all the photos out of specified directory and rename them into a new directory structure.


## Installation

    gem install photoarchiver

## Usage
I use Photoarchiver from IRB.  I simply fire up IRB, require the gem and invoke it like so:

	$ irb
	$ 2.0.0p353 :001 > require 'photoarchiver'
	$ 2.0.0p353 :002 > Photoarchiver.organize('/Users/you/pics/source', '/Users/you/Pictures')
	...
	$ 2.0.0p353 :003 > exit
	

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request


## My Workflow

I typically download my photos from my different cameras (canon, iphone, ipad, etc).  Then, I organize them and rename them so I can place them in a folder in DropBox.  I got tired of doing this manually.  I also didn't find another tool (though it may be still out there) that did this in the structure I like.

I wanted a utility that could read all the pictures, recursively, out of the folder I just downloaded from my camera, etc.  Then, I wanted it to take that list and create folders based on the YEAR of the EXIF data of the picture.  Under the year folder I wanted to create folders that had YEAR-MONTH-DAY folders.  Then, place the pictures in the appropriate places.