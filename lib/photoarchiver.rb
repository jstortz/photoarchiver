require 'photoarchiver/version'
require 'exifr'
require 'fileutils'

module Photoarchiver

  def self.organize(dir_src, dir_target)

    puts "Photoarchiver #{Photoarchiver::VERSION}"

    # Simply check to see if the source directory exists and complain if it does not
    if File.exists?(dir_src) && File.directory?(dir_src)
      # Source dir exists, go ahead and make target directory if it does not exist
      Dir.mkdir(dir_target) unless File.exists?(dir_target)
    else
      raise 'First argument, source directory, does not exist or is not a directory'
    end

    puts "Source Directory:  #{dir_src}"
    puts "Target Directory:  #{dir_target}"


    # Gather up all the potential photo files
    files = Dir.glob(File.join(dir_src, '**', '*')).reject { |temp| File.directory? temp }
    photos = files.grep(/\.JPG/i)

    # Loop through the photos, not currently using index but may in the future
    photos.each_with_index {|val, index|

      date_created = EXIFR::JPEG.new(val).exif.date_time_original || File.stat(val).ctime

      FileUtils::mkdir_p File.join(dir_target, date_created.strftime('%Y'), date_created.strftime('%Y-%m-%d'))
      target_file = File.join(dir_target, date_created.strftime('%Y'), date_created.strftime('%Y-%m-%d'), date_created.strftime('%Y%m%d-%H%M%S')) + '.jpg'
      puts "Moving #{val} to #{target_file}"
      File.rename(val, target_file)
    }
    puts 'Done processing photos'
  end
end