# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'photoarchiver/version'

Gem::Specification.new do |spec|
  spec.name          = 'photoarchiver'
  spec.version       = Photoarchiver::VERSION
  spec.authors       = ['Jason Stortz']
  spec.email         = ['jstortz@redstonecontentsolutions.com']
  spec.description   = 'This gem organizes photos into a series of directories.  See the README.md file for more information.'
  spec.summary       = 'This gem creates directories based on year, date and month and places photos in them'
  spec.homepage      = 'https://bitbucket.org/jstortz/photoarchiver'
  spec.license       = 'MIT'

  spec.files         = `git ls-files | grep -v .idea`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.3'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'exifr'
  spec.add_development_dependency 'fileutils'
end
